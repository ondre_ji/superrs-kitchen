#!/bin/sh
# AIK-Linux/repackimg: repack ramdisk and build image
# osm0sis @ xda-developers
# Modified with permission for SuperR's Kitchen

abort() { cd "$PWD"; echo "Error!"; }

bin="$PWD/bin";
chmod -R 755 "$bin" "$PWD"/*.sh;
chmod 644 "$bin/magic";
# START Changes for SuperR's Kitchen
# cd "$PWD";
cd "$romdir";
# END Changes for SuperR's Kitchen

arch=`uname -m`;

# START Changes for SuperR's Kitchen
#if [ -z "$(ls split_img/* 2> /dev/null)" -o -z "$(ls ramdisk/* 2> /dev/null)" ]; then
if [ -z "$(ls $romdir/recoveryimg/split_img/* 2> /dev/null)" -o -z "$(ls $romdir/recoveryimg/ramdisk/* 2> /dev/null)" ]; then
# END Changes for SuperR's Kitchen
  echo "No files found to be packed/built.";
  abort;
  exit 1;
fi;

clear;
echo " ";
echo "Android Image Kitchen - RepackImg Script";
echo "by osm0sis @ xda-developers";
echo " ";

# START Changes for SuperR's Kitchen
# if [ ! -z "$(ls *-new.* 2> /dev/null)" ]; then
if [ ! -z "$(ls $romdir/recoveryimg/*-new.* 2> /dev/null)" ]; then
# END Changes for SuperR's Kitchen
  echo "Warning: Overwriting existing files!";
  echo " ";
fi;

# START Changes for SuperR's Kitchen
#rm -f ramdisk-new.cpio*;
rm -f $romdir/recoveryimg/ramdisk-new.cpio*;
# END Changes for SuperR's Kitchen
case $1 in
  --original)
    echo "Repacking with original ramdisk...";;
  --level|*)
    echo "Packing ramdisk...";
    echo " ";
    # START Changes for SuperR's Kitchen
    # ramdiskcomp=`cat split_img/*-ramdiskcomp`;
    ramdiskcomp=`cat $romdir/recoveryimg/split_img/*-ramdiskcomp`;
    # END Changes for SuperR's Kitchen
    if [ "$1" = "--level" -a "$2" ]; then
      level="-$2";
      lvltxt=" - Level: $2";
    elif [ "$ramdiskcomp" = "xz" ]; then
      level=-1;
    fi;
    echo "Using compression: $ramdiskcomp$lvltxt";
    repackcmd="$ramdiskcomp $level";
    compext=$ramdiskcomp;
    case $ramdiskcomp in
      gzip) compext=gz;;
      lzop) compext=lzo;;
      xz) repackcmd="xz $level -Ccrc32";;
      lzma) repackcmd="xz $level -Flzma";;
      bzip2) compext=bz2;;
      lz4) repackcmd="$bin/$arch/lz4 $level -l stdin stdout";;
    esac;
    # START Changes for SuperR's Kitchen
    # cd ramdisk;
    cd $romdir/recoveryimg/ramdisk;
    #find . | cpio -H newc -o 2> /dev/null | $repackcmd > ../ramdisk-new.cpio.$compext;
    find . | cpio -H newc -o 2> /dev/null | $repackcmd > $romdir/recoveryimg/ramdisk-new.cpio.$compext;
    # END Changes for SuperR's Kitchen
    if [ ! $? -eq "0" ]; then
      abort;
      exit 1;
    fi;
    cd ..;;
esac;

echo " ";
echo "Getting build information...";
# START Changes for SuperR's Kitchen
# cd split_img;
cd $romdir/recoveryimg/split_img;
kernel=`ls *-zImage`;               echo "kernel = $kernel";
if [ "$1" = "--original" ]; then
  ramdisk=`ls *-ramdisk.cpio*`;     echo "ramdisk = $ramdisk";
  # ramdisk="split_img/$ramdisk";
  ramdisk="$romdir/recoveryimg/split_img/$ramdisk";

else
  # ramdisk="ramdisk-new.cpio.$compext";
  ramdisk="$romdir/recoveryimg/ramdisk-new.cpio.$compext";
fi;
# END Changes for SuperR's Kitchen
cmdline=`cat *-cmdline`;            echo "cmdline = $cmdline";
board=`cat *-board`;                echo "board = $board";
base=`cat *-base`;                  echo "base = $base";
pagesize=`cat *-pagesize`;          echo "pagesize = $pagesize";
kerneloff=`cat *-kerneloff`;        echo "kernel_offset = $kerneloff";
ramdiskoff=`cat *-ramdiskoff`;      echo "ramdisk_offset = $ramdiskoff";
tagsoff=`cat *-tagsoff`;            echo "tags_offset = $tagsoff";
if [ -f *-second ]; then
  second=`ls *-second`;             echo "second = $second";  
  second="--second split_img/$second";
  secondoff=`cat *-secondoff`;      echo "second_offset = $secondoff";
  secondoff="--second_offset $secondoff";
fi;
if [ -f *-dtb ]; then
  dtb=`ls *-dtb`;                   echo "dtb = $dtb";
  # START Changes for SuperR's Kitchen
  # dtb="--dt split_img/$dtb";
  dtb="--dt $romdir/recoveryimg/split_img/$dtb";
  # END Changes for SuperR's Kitchen
fi;
# START Changes for SuperR's Kitchen
# cd ..;
cd $romdir;
# END Changes for SuperR's Kitchen

echo " ";
echo "Building image...";
echo " ";
# START Changes for SuperR's Kitchen
# $bin/$arch/mkbootimg --kernel "split_img/$kernel" --ramdisk "$ramdisk" $second --cmdline "$cmdline" --board "$board" --base $base --pagesize $pagesize --kernel_offset $kerneloff --ramdisk_offset $ramdiskoff $secondoff --tags_offset $tagsoff $dtb -o image-new.img;
$bin/$arch/mkbootimg --kernel "$romdir/recoveryimg/split_img/$kernel" --ramdisk "$ramdisk" $second --cmdline "$cmdline" --board "$board" --base $base --pagesize $pagesize --kernel_offset $kerneloff --ramdisk_offset $ramdiskoff $secondoff --tags_offset $tagsoff $dtb -o image-new.img;
# END Changes for SuperR's Kitchen
if [ ! $? -eq "0" ]; then
  abort;
  exit 1;
fi;

echo "Done!";
exit 0;

