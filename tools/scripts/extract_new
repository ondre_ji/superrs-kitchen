#!/bin/bash

# Do not edit this file unless you know what you are doing

banner() {
	echo ""
	echo "------------------------------------------------------"
	echo -e "-\e[1;107;30m                 SuperR's Kitchen                   \e[0m-"
	echo -e "-\e[3;107;30m                     by SuperR                      \e[0m-"
	echo "------------------------------------------------------"
	echo ""
}
cd $base
if [[ $(ls -d */ | grep 'superr_') = "" ]]; then
	clear
	banner
	echo "${redb}${yellowt}${bold}ERROR:$normal"
	echo "${redt}There are no projects to extract from.$normal"
	echo ""
	read -p "Press ENTER to return to then Main menu"
	cd $base
	exec ./superr
	exit			
fi
adb=$tools/adb
timestamp=$(date +%m-%d-%Y-%H:%M:%S)
cd $romdir
romzip=""
romtar=""
romimg=""
ls | grep .zip >> findextract
ls | grep .tar >> findextract
ls | grep .img | grep -v boot | grep -v recovery >> findextract
if [[ ! $(cat findextract | wc -l) -ge "2" ]]; then
	if [[ ! $(cat findextract | grep .zip) = "" ]]; then
		romzip=$(cat findextract)
	elif [[ ! $(cat findextract | grep .tar) = "" ]]; then
		romtar=$(cat findextract)
	elif [[ ! $(cat findextract | grep .img) = "" ]]; then
		romimg=$(cat findextract)
	fi
	rm -rf $romdir/findextract
else	
	clear
	banner
	echo "${greenb}Extract Options:$normal"
	echo ""
	COLUMNS="20"
	PS3="
Choose an option and press ENTER: "
	findextract=( $(cat $romdir/findextract) )
	select project in "${findextract[@]}" "Return to Main menu" ; do
		if (( REPLY == 1 + ${#findextract[@]} )) ; then
			rm -rf $romdir/findextract
			cd $base
			exec ./superr
			exit
		elif (( REPLY > 0 && REPLY <= ${#findextract[@]} )) ; then
			if [[ ! $(echo $project | grep .zip) = "" ]] ; then
				romzip="$project"
			elif [[ ! $(echo $project | grep .tar) = "" ]] ; then
				romtar="$project"
			elif [[ ! $(echo $project | grep .img) = "" ]] ; then
				romimg="$project"
			fi
			rm -rf $romdir/findextract
			break
		else
			echo "${redt}Invalid option. Please try again.$normal"
		fi
	done
fi
if [[ $romzip = "" && $romtar = "" && $romimg = "" ]]; then	
	choice=""
	while [[ $choice -lt "1" || $choice -gt "3" ]] ; do
		clear
		banner
		echo "${bluet}CURRENT PROJECT: $greent$romname$normal"
		echo ""
		echo "${yellowb}${redt}Extract Menu$normal"
		echo ""
		echo "1) Add a ROM zip, tar/boot.img, or system.img/boot.img"
		echo "   then choose this option"
		echo "2) Pull system, boot, and recovery img's from your ${redt}${bold}rooted$normal"
		echo "   device for extraction. ${yellowt}${bold}NOTE:$normal recovery.img will be placed"
		echo "   in the device directory, not in the ROM directory"
		echo "3) Main Menu"
		echo ""
		read -n 1 -p "Please enter your choice:" choice
	done

	# START I added a ROM zip, tar/boot.img, or system.img/boot.img
	if [[ $choice == "1" ]]; then
		cd $scripts
		exec ./extract_new
		exit
	# START Extract system.img and boot.img from your rooted device
	elif [[ $choice == "2" ]]; then
		clear
		banner
		echo "${bluet}** Enable usb debugging on your Android device in system settings"
		echo "** Plug in your device$normal"
		echo ""
		read -p "Press ENTER when ready"
		clear
		banner
		echo "${bluet}Detecting partition info ...$normal"
		echo ""
		$adb "wait-for-device"
		$adb pull /system/build.prop $romdir/build.prop
		devicename=$(cat $romdir/build.prop | grep "ro.product.device=" | cut -d'=' -f2)
		mkdir $base/devices/$devicename
		byname1=$($adb shell "mount | grep by-name | grep system")
		byname=$(echo "$byname1" | awk '{ print $1 }' | rev | cut -d'/' -f2- | rev)
		echo "$byname1" | awk '{ print $1 }' | rev | cut -d'/' -f2- | rev | sed 's:/:\\/:g' > $base/devices/$devicename/superr_byname
		clear
		banner
		echo "${bluet}Extracting boot.img from your device ...$normal"
		echo ""
		$adb pull $byname/boot $romdir/boot.img > $logs/main.log
		clear
		banner
		echo "${bluet}Extracting system.img from your device ...$normal"
		echo ""
		$adb pull $byname/system $romdir/system.img > $logs/main.log
		clear
		banner
		echo "${bluet}Extracting recovery.img from your device ...$normal"
		echo ""
		$adb pull $byname/recovery $base/devices/$devicename/recovery.img > $logs/main.log
		rm -rf $romdir/build.prop
		if [[ ! -f $romdir/system.img ]]; then
			clear
			banner
			echo "${bluet}Extracting system partition to /sdcard ...$normal"
			echo ""
			$adb shell su -C "dd if=$byname/system of=/sdcard/system.img"
			echo ""
			echo "${bluet}Pulling system.img from your device ...$normal"
			$adb pull /sdcard/system.img $romdir/system.img > $logs/main.log
			$adb shell "rm -rf /sdcard/system.img"
			clear
			banner
			echo "${bluet}Extracting boot partition to /sdcard ...$normal"
			echo ""
			$adb shell su -C "dd if=$byname/boot of=/sdcard/boot.img"
			echo ""
			echo "${bluet}Pulling boot.img from your device ...$normal"
			$adb pull /sdcard/boot.img $romdir/boot.img > $logs/main.log
			$adb shell "rm -rf /sdcard/boot.img"
			clear
			banner
			echo "${bluet}Extracting recovery partition to /sdcard ...$normal"
			echo ""
			$adb shell su -C "dd if=$byname/recovery of=/sdcard/recovery.img"
			echo ""
			echo "${bluet}Pulling recovery.img from your device ...$normal"
			$adb pull /sdcard/recovery.img $base/devices/$devicename/recovery.img > $logs/main.log
			$adb shell "rm -rf /sdcard/recovery.img"
		fi
		if [[ -f $romdir/system.img ]]; then
			romimg="system.img"
		else	
			clear
			banner
			echo "${redb}${yellowt}${bold}ERROR:$normal"
			echo "${redt}Something went wrong. Most likely you lack permission"
			echo "to pull img's from your device.$normal"
			echo ""
			read -p "Press ENTER to return to then Main menu"
			cd $base
			exec ./superr
			exit			
		fi
	# START Main Menu
	elif [[ $choice == "3" ]]; then	
		cd $base
		exec ./superr
		exit	
	fi
fi
if [[ ! $romzip = "" ]]; then	
	if [[ $(7za l $romzip | grep "META-INF/com/google/android/updater-script") = "" && $(7za l $romzip | grep "system.ext4.tar") = "" ]]; then
		clear
		banner
		echo "${redb}${yellowt}${bold}WARNING:$normal"
		echo "${redt}This zip does not contain an updater-script"
		echo "and will not create a valid ROM base.$normal"
		echo ""
		read -n 1 -p "Would you like to continue anyway? y/n  "
		if [[ ! $REPLY = "y" ]]; then
			cd $base
			exec ./superr
			exit
		fi			
	fi			
fi
if [[ $romimg = "" ]]; then
	romwin=$(ls | grep .win | grep -v boot)
	romtara=$(ls | grep .tar.a)
	if [[ ! $romwin = "" ]]; then
		romwintar=$(echo $romwin | sed 's/win/tar/')
		mv $romwin $romwintar
		if [[ -f boot.emmc.win ]]; then
			mv boot.emmc.win boot.img
		fi
	fi
	if [[ ! $romtara = "" ]]; then
		romtaratar=$(echo $romtara | sed 's/tar\.a/tar/')
		mv $romtara $romtaratar
	fi
fi
clear
banner
if [[ ! $romwin = "" ]]; then
echo "$bluet$romwin was renamed to $romtar$normal"
echo ""
fi
if [[ ! $romtara = "" ]]; then
echo "$bluet$romtara was renamed to $romtar$normal"
echo ""
fi
read -n 1 -p "Should we extract $yellowt$romzip$romtar$romimg$normal to ${greent}$romname$normal? y/n  "
echo ""
echo ""
if [[ $REPLY = "y" ]]; then
	if [[ -d $romdir/system || -d $romdir/META-INF || -d $romdir/supersu || -d $romdir/data ]]; then
		echo $timestamp > $romdir/temptime
		temptime=$(cat $romdir/temptime)
		mkdir -p $base/old_rom_files/$romname.$temptime
	fi
	if [[ -d $romdir/system ]]; then
		mv $romdir/system $base/old_rom_files/$romname.$temptime/
	fi
	if [[ -d $romdir/META-INF ]]; then
		mv $romdir/META-INF $base/old_rom_files/$romname.$temptime/
	fi
	if [[ -d $romdir/supersu ]]; then
		mv $romdir/supersu $base/old_rom_files/$romname.$temptime/
	fi
	if [[ -d $romdir/data ]]; then
		mv $romdir/data $base/old_rom_files/$romname.$temptime/
	fi
	if [[ ! $romzip = "" && ! $(7za l $romdir/$romzip | grep boot.img) = "" ]]; then	
		if [[ -f $romdir/boot.img ]]; then
			if [[ ! -d $base/old_rom_files/$romname.$temptime ]]; then
				echo $timestamp > $romdir/temptime
				temptime=$(cat $romdir/temptime)
				mkdir -p $base/old_rom_files/$romname.$temptime
			fi
			mv $romdir/boot.img $base/old_rom_files/$romname.$temptime/
		fi
	fi
	if [[ -d $base/old_rom_files/$romname.$temptime ]]; then
		movelist=$(ls $base/old_rom_files/$romname.$temptime)
		clear
		banner
		echo "${bluet}The following have been moved to:"
		echo "$yellowt$base/old_rom_files/$romname.$temptime $normal"
		echo ""
		echo "$yellowt$movelist$normal"
		echo ""
		rm -rf $romdir/temptime
		read -p "Press ENTER to continue extracting $romzip$romtar$romimg ..."
	fi	
	if [[ ! $romzip = "" ]]; then
		if [[ ! $(7za l $romdir/$romzip | grep system.new.dat) = "" ]]; then
			cd $romdir
			clear
			banner
			echo "${bluet}Extracting system.new.dat, system.transfer.list, and boot.img ...$normal"
			7za e $romzip system.new.dat system.transfer.list boot.img >> $logs/zip.log
			clear
			banner
			echo "${bluet}Converting to system.img ...$normal"
			$tools/sdat2img.py system.transfer.list system.new.dat system.img > $logs/main.log
			rm -rf system.transfer.list system.new.dat
			romzip=""
			romimg="system.img"
		fi
		if [[ ! $(7za l $romzip | grep system.ext4.tar.a) = "" ]]; then
			clear
			banner
			echo "${bluet}Extracting system.ext4.tar.a and boot.img ...$normal"
			7za e $romzip system.ext4.tar.a boot.img >> $logs/zip.log
			romzip=""
			romtar="system.ext4.tar.a"
		fi			
	fi
	if [[ ! $romimg = "" ]]; then
		romimgdir=$(echo $romimg | rev | cut -d'.' -f2- | rev)
		myuser=$(logname)
		sparse=$(file $romimg | grep sparse)
		if [[ ! $sparse = "" ]]; then
			clear
			banner
			echo "${bluet}Converting $romimg from sparse to raw ...$normal"
			$tools/simg2img $romimg $romimg-2
			mv $romimg-2 $romimg
		fi
		clear
		banner
		echo "${bluet}Mounting $romimg ...$normal"
		mkdir output
		echo ""
		sudo mount -t ext4 -o loop $romimg output/
		clear
		banner
		echo "${bluet}Copying files to $romimgdir ..."
		echo ""
		echo "The directory will be renamed to system if it is a system.img$normal"
		mkdir $romimgdir
		cd $romdir
		sudo cp -r ./output/* $romdir/$romimgdir
		sudo umount output/
		sudo chown -hR $myuser:$myuser $romdir/$romimgdir
		rm -rf $romdir/output
		if [[ -f $romdir/$romimgdir/build.prop ]]; then
			mv $romdir/$romimgdir $romdir/system
		fi
		romtar="Yes"
	fi	
	if [[ ! $romzip = "" ]]; then	
		clear
		banner
		echo "${bluet}Extracting $romzip in $romname$normal"
		7za x $romzip >> $logs/zip.log
		devicename=""
		deviceloc=""
		if [[ -f $romdir/system/build.prop && -f $romdir/META-INF/com/google/android/updater-script ]]; then
			export devicename=$(cat $romdir/system/build.prop | grep "ro.product.device=" | cut -d"=" -f2)
			mkdir -p $base/devices/$devicename/$romname
			export deviceloc=$base/devices/$devicename
			cd $romdir/META-INF/com/google/android
			cat updater-script | grep -m 1 "^mount" | awk '{ print $3 }' | sed 's/\"//g' | sed 's/\/system//g' | sed 's/\,//g' | sed 's:\/:\\/:g' > $deviceloc/superr_byname
			export permtype=""
			if [[ ! $(grep ^set_perm updater-script) = "" ]]; then
				export permtype="set_perm"
				echo "$permtype" > $deviceloc/$romname/permtype
				grep ^$permtype updater-script > $deviceloc/$romname/$permtype
			elif [[ ! $(grep ^set_metadata updater-script) = "" ]]; then
				export permtype="set_metadata"
				echo "$permtype" > $deviceloc/$romname/permtype
				grep ^$permtype updater-script > $deviceloc/$romname/$permtype
			fi
			grep ^symlink updater-script > $deviceloc/$romname/symlinks
			rm -rf $deviceloc/$romname/debloat_test
		fi
	elif [[ ! $romtar = "" ]]; then
		if [[ $romimg = "" ]]; then
			if [[ ! $(tar --numeric-owner -tvf $romtar 2>&1 | awk '{ print $2, $6 }' | grep -m 1 "/bin" | awk '{ print $1 }') = "0/2000" ]]; then
				clear
				banner
				echo "${redb}${yellowt}${bold}ERROR:$normal"
				echo "${redt}Looks like the tar file you have is not from a nandroid backup$normal"
				echo ""
				read -p "Press ENTER to return to the main menu"
				cd $base
				exec ./superr
				exit
			fi
			clear
			banner
			echo "${bluet}Extracting $romtar in $romname$normal"
			cd $romdir
			if [[ ! $(tar -tf $romtar -P 2>&1 | grep -o -m 1 system/) = "system/" ]]; then
				mkdir system
				tar -xf $romtar -C system >/dev/null 2>&1 >> $logs/zip.log
			else 
				tar -xf $romtar >/dev/null 2>&1 >> $logs/zip.log
			fi
		fi
		if [[ -d $romdir/system ]]; then	
			clear
			banner
			echo "${bluet}Preparing project directory ...$normal"
			cp -r $tools/updater/META-INF $romdir
			export devicename=""
			export devicename=$(cat $romdir/system/build.prop | grep "ro.product.device=" | sed 's/ro\.product\.device=//')
			mkdir -p $base/devices/$devicename/$romname
			deviceloc=""
			export deviceloc=$base/devices/$devicename
			cd $romdir
			find . -type l -printf "%l\n" | sed 's/^/symlink(\"/' | sed 's/$/\", /' > $deviceloc/$romname/sym1.txt
			find . -type l | sed 's/\./\"/' | sed 's/$/\");/' > $deviceloc/$romname/sym2.txt
			cd $deviceloc/$romname
			paste -d '' sym1.txt sym2.txt > sym3.txt
			cat sym3.txt | sort > symlinks
			rm -rf sym1.txt sym2.txt sym3.txt
			rm -rf debloat_test
			cd $romdir
			find . -type l -exec rm -f {} \;
			cd $scripts
			./update_perms
		fi
	fi
fi
cd $base
exec ./superr
exit
